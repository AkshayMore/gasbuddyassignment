package com.moreakshay.gasbuddyassignment.injection.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.moreakshay.gasbuddyassignment.ui.photos.viewmodels.PhotoDetailsViewModel
import com.moreakshay.gasbuddyassignment.ui.photos.viewmodels.PhotoListViewModel
import com.moreakshay.gasbuddyassignment.utils.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.moreakshay.gasbuddyassignment.injection.keys.ViewModelKey

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PhotoListViewModel::class)
    abstract fun bindPhotoListViewModel(photoListViewModel: PhotoListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoDetailsViewModel::class)
    abstract fun bindPhotoDetailsViewModel(photoDetailsViewModel: PhotoDetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

}