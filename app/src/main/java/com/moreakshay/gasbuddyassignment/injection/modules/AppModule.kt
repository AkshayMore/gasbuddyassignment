package com.moreakshay.gasbuddyassignment.injection.modules

import android.content.Context
import com.moreakshay.gasbuddyassignment.GasBuddyApplication
import dagger.Module
import dagger.Provides
import com.moreakshay.gasbuddyassignment.injection.qualifiers.ApplicationContext
import com.moreakshay.gasbuddyassignment.injection.scopes.ApplicationScope

@Module
class AppModule(var mineApp: GasBuddyApplication) {

    @ApplicationContext
    @ApplicationScope
    @Provides
    fun getContext(): Context{
        return mineApp
    }
}