package com.moreakshay.gasbuddyassignment.injection.modules

import com.moreakshay.gasbuddyassignment.BuildConfig
import com.moreakshay.gasbuddyassignment.data.remote.ApiService
import com.moreakshay.gasbuddyassignment.utils.constants.AUTHORIZATION
import com.moreakshay.gasbuddyassignment.utils.constants.BASE_URL
import dagger.Module
import dagger.Provides
import com.moreakshay.gasbuddyassignment.injection.scopes.ApplicationScope
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class RepositoryModule {

    @ApplicationScope
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @ApplicationScope
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
    }

    @ApplicationScope
    @Provides
    fun provideHttpClient(interceptor: Interceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
    }

    @ApplicationScope
    @Provides
    fun provideHeaderInterceptor(): Interceptor {
        return Interceptor {
            val request = it.request()
                    .newBuilder()
                    .addHeader(AUTHORIZATION, BuildConfig.AUTHORIZATION)
                    .build()
            return@Interceptor it.proceed(request)
        }
    }

    /*@ApplicationScope
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): MineDatabase {
        return Room.databaseBuilder(context, MineDatabase::class.java, DATABASE_NAME).build()
    }*/
}