package com.moreakshay.gasbuddyassignment.injection.component

import android.content.Context
import com.moreakshay.gasbuddyassignment.GasBuddyApplication
import com.moreakshay.gasbuddyassignment.data.remote.GasBuddyRepository
import com.moreakshay.gasbuddyassignment.ui.photos.PhotoListActivity
import com.moreakshay.gasbuddyassignment.ui.photos.PhotoDetailsActivity
import dagger.Component
import com.moreakshay.gasbuddyassignment.injection.modules.AppModule
import com.moreakshay.gasbuddyassignment.injection.modules.RepositoryModule
import com.moreakshay.gasbuddyassignment.injection.modules.ViewModelModule
import com.moreakshay.gasbuddyassignment.injection.qualifiers.ApplicationContext
import com.moreakshay.gasbuddyassignment.injection.scopes.ApplicationScope

@ApplicationScope
@Component(modules = [AppModule::class, ViewModelModule::class, RepositoryModule::class])
interface AppComponent {

    @ApplicationContext
    fun getContext(): Context

    fun getRepository(): GasBuddyRepository

    fun inject(app: GasBuddyApplication)

    fun inject(activity: PhotoListActivity)

    fun inject(activity: PhotoDetailsActivity)

}