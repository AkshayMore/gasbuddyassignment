package com.moreakshay.gasbuddyassignment.ui.domain

data class PhotoDetails ( val id: String,
                            val url: String,
                            val name: String,
                            val location: String,
                            val date: String,
                            val likes: Int,
                            val description: String)