package com.moreakshay.gasbuddyassignment.ui.photos.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.moreakshay.gasbuddyassignment.data.remote.GasBuddyRepository
import com.moreakshay.gasbuddyassignment.ui.domain.PhotoDetails
import kotlinx.coroutines.Dispatchers
import com.moreakshay.gasbuddyassignment.utils.network.Resource
import java.lang.IllegalArgumentException
import javax.inject.Inject

class PhotoDetailsViewModel @Inject constructor(private val repository: GasBuddyRepository) :
    ViewModel() {

    lateinit var id: String
    val photoDetails: LiveData<Resource<PhotoDetails>> by lazy {
        if (id.isNotEmpty()) getPhotoDetails(id)
        else throw IllegalArgumentException()
    }

    private fun getPhotoDetails(id: String): LiveData<Resource<PhotoDetails>> =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emitSource(repository.getPhotoById(id))
        }
}