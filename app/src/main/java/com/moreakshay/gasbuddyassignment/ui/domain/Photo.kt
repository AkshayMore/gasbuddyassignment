package com.moreakshay.gasbuddyassignment.ui.domain

data class Photo( val id: String, val url: String, val name: String)