package com.moreakshay.gasbuddyassignment.ui.photos.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.moreakshay.gasbuddyassignment.data.remote.GasBuddyRepository
import com.moreakshay.gasbuddyassignment.ui.domain.Photo
import kotlinx.coroutines.Dispatchers
import com.moreakshay.gasbuddyassignment.utils.network.Resource
import javax.inject.Inject

class PhotoListViewModel @Inject constructor(private val repository: GasBuddyRepository) : ViewModel() {

    val photoList: LiveData<Resource<List<Photo>>> =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emitSource(repository.getPhotos())
        }
}