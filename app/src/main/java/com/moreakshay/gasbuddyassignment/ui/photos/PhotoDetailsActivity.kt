package com.moreakshay.gasbuddyassignment.ui.photos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.moreakshay.gasbuddyassignment.GasBuddyApplication
import com.moreakshay.gasbuddyassignment.R
import com.moreakshay.gasbuddyassignment.databinding.ActivityPhotoDetailsBinding
import com.moreakshay.gasbuddyassignment.ui.photos.viewmodels.PhotoDetailsViewModel
import com.moreakshay.gasbuddyassignment.utils.constants.ID
import com.moreakshay.gasbuddyassignment.utils.view.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject

class PhotoDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewmodel: PhotoDetailsViewModel by viewModels { viewModelFactory }

    private val binding: ActivityPhotoDetailsBinding by lazy {
        DataBindingUtil.setContentView<ActivityPhotoDetailsBinding>(
            this,
            R.layout.activity_photo_details
        )
    }

    private val id: String by lazy { intent.getStringExtra(ID) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        (application as GasBuddyApplication).component.inject(this)
        viewmodel.id = id
        binding.photoDetails = viewmodel.photoDetails
        setupView()
    }

    private fun setupView() {
        toolbar.bBack.visibility = View.VISIBLE
        toolbar.bBack.setOnClickListener { onBackPressed() }
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }
}