package com.moreakshay.gasbuddyassignment.ui.photos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moreakshay.gasbuddyassignment.GasBuddyApplication
import com.moreakshay.gasbuddyassignment.R
import com.moreakshay.gasbuddyassignment.databinding.ActivityMainBinding
import com.moreakshay.gasbuddyassignment.ui.ClickListener
import com.moreakshay.gasbuddyassignment.ui.PhotoAdapter
import com.moreakshay.gasbuddyassignment.ui.photos.viewmodels.PhotoListViewModel
import com.moreakshay.gasbuddyassignment.utils.constants.ID
import com.moreakshay.gasbuddyassignment.utils.view.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class PhotoListActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: PhotoListViewModel by viewModels {viewModelFactory}
    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    private val adapter: PhotoAdapter = PhotoAdapter(ClickListener { photo ->
        val intent = Intent(this, PhotoDetailsActivity::class.java)
        intent.putExtra(ID, photo.id)
        startActivity(intent)
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        (application as GasBuddyApplication).component.inject(this)
        bind()
        setupView()
    }

    private fun setupView() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val decor = SimpleDividerItemDecoration(this)
        rvPhotos.addItemDecoration(decor)
    }

    private fun bind() {
        binding.photoList = viewModel.photoList
        binding.rvPhotos.adapter = adapter
    }
}
