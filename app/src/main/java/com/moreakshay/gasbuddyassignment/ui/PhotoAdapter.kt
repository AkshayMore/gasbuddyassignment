package com.moreakshay.gasbuddyassignment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.moreakshay.gasbuddyassignment.R
import com.moreakshay.gasbuddyassignment.databinding.ItemPhotoBinding
import com.moreakshay.gasbuddyassignment.ui.domain.Photo

class PhotoAdapter(private val clickListener: ClickListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem.url == newItem.url
        }
    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)

    fun submitList(list: List<Photo>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PhotoViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_photo, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PhotoViewHolder).bind(differ.currentList[position], clickListener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}

class PhotoViewHolder(private var binding: ItemPhotoBinding) : RecyclerView.ViewHolder(binding.root){
    fun bind(photo: Photo, clickListener: ClickListener){
        binding.photo = photo
        binding.clickListener = clickListener
    }
}

class ClickListener constructor( val clickListener: (photo: Photo) -> Unit) {
    fun onClick(photo: Photo) = clickListener(photo)
}