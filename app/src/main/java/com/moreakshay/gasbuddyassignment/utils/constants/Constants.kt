package com.moreakshay.gasbuddyassignment.utils.constants

//API CONSTANTS
const val BASE_URL = "https://api.unsplash.com/"
const val POPULAR = "popular"
const val AUTHORIZATION = "Authorization"

//APP CONSTANTS
const val NOT_AVAILABLE = "N/A"
const val ID = "ID"
