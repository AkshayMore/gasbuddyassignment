package com.moreakshay.gasbuddyassignment.utils.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers

abstract class NetworkBoundResource<ResultType : Any, RequestType : Any> {

    private val responseHandler = ResponseHandler()

    fun makeNetworkCall(): LiveData<Resource<ResultType>> {
        return liveData(Dispatchers.IO) {
            emit(responseHandler.handleLoading(null))
            try {
                val apiResponse = createCall()
                apiResponse.let {
                    var result = toDomainType(apiResponse)
                    emit(responseHandler.handleSuccess(result))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emit(responseHandler.handleException(e, null))
            }
        }
    }

    protected abstract suspend fun toDomainType(item: RequestType): ResultType

    protected abstract suspend fun createCall(): RequestType
}