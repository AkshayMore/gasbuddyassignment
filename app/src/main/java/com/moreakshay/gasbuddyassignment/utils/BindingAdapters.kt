package com.moreakshay.gasbuddyassignment.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.moreakshay.gasbuddyassignment.R
import com.moreakshay.gasbuddyassignment.ui.PhotoAdapter
import com.moreakshay.gasbuddyassignment.ui.domain.Photo


@BindingAdapter(value = ["imageUrl"], requireAll = false)
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        Glide.with(imgView.context)
                .load(imgUrl)
                .apply(RequestOptions()
                        .placeholder(R.drawable.poster_placeholder) //TODO: add animation-rotate resources from codelabs
                        .error(R.drawable.poster_placeholder))
                .into(imgView)
    }
}

@BindingAdapter("listData")
fun  bindRecyclerView(recyclerView: RecyclerView, showList: List<Photo>?) =
        showList?.let { (recyclerView.adapter as PhotoAdapter).submitList(it) }
