package com.moreakshay.gasbuddyassignment.data.remote

import com.moreakshay.gasbuddyassignment.data.dtos.PhotoDetailsResponse
import com.moreakshay.gasbuddyassignment.data.dtos.PhotosResponse
import com.moreakshay.gasbuddyassignment.utils.constants.POPULAR
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("photos")
    suspend fun getPhotos(@Query("order_by") orderBy: String = POPULAR): List<PhotosResponse.PhotosResponseItem>

    @GET("photos/{id}")
    suspend fun getPhotoDetails(@Path("id") id: String): PhotoDetailsResponse
}