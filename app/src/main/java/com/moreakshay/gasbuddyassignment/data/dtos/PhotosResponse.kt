package com.moreakshay.gasbuddyassignment.data.dtos


import com.moreakshay.gasbuddyassignment.ui.domain.Photo
import com.squareup.moshi.Json

class PhotosResponse  {

    data class PhotosResponseItem(
        @field:Json(name = "alt_description")
        val altDescription: String,
        @field:Json(name = "categories")
        val categories: List<Any>,
        @field:Json(name = "color")
        val color: String,
        @field:Json(name = "created_at")
        val createdAt: String,
        @field:Json(name = "current_user_collections")
        val currentUserCollections: List<Any>,
        @field:Json(name = "description")
        val description: String,
        @field:Json(name = "height")
        val height: Int,
        @field:Json(name = "id")
        val id: String,
        @field:Json(name = "liked_by_user")
        val likedByUser: Boolean,
        @field:Json(name = "likes")
        val likes: Int,
        @field:Json(name = "links")
        val links: Links,
        @field:Json(name = "promoted_at")
        val promotedAt: String,
        @field:Json(name = "sponsorship")
        val sponsorship: Sponsorship,
        @field:Json(name = "updated_at")
        val updatedAt: String,
        @field:Json(name = "urls")
        val urls: Urls,
        @field:Json(name = "user")
        val user: User,
        @field:Json(name = "width")
        val width: Int
    ) {
        data class Links(
            @field:Json(name = "download")
            val download: String,
            @field:Json(name = "download_location")
            val downloadLocation: String,
            @field:Json(name = "html")
            val html: String,
            @field:Json(name = "self")
            val self: String
        )
    
        data class Sponsorship(
            @field:Json(name = "impression_urls")
            val impressionUrls: List<Any>,
            @field:Json(name = "sponsor")
            val sponsor: Sponsor,
            @field:Json(name = "tagline")
            val tagline: String,
            @field:Json(name = "tagline_url")
            val taglineUrl: Any
        ) {
            data class Sponsor(
                @field:Json(name = "accepted_tos")
                val acceptedTos: Boolean,
                @field:Json(name = "bio")
                val bio: String,
                @field:Json(name = "first_name")
                val firstName: String,
                @field:Json(name = "id")
                val id: String,
                @field:Json(name = "instagram_username")
                val instagramUsername: String,
                @field:Json(name = "last_name")
                val lastName: Any,
                @field:Json(name = "links")
                val links: Links,
                @field:Json(name = "location")
                val location: Any,
                @field:Json(name = "name")
                val name: String,
                @field:Json(name = "portfolio_url")
                val portfolioUrl: String,
                @field:Json(name = "profile_image")
                val profileImage: ProfileImage,
                @field:Json(name = "total_collections")
                val totalCollections: Int,
                @field:Json(name = "total_likes")
                val totalLikes: Int,
                @field:Json(name = "total_photos")
                val totalPhotos: Int,
                @field:Json(name = "twitter_username")
                val twitterUsername: String,
                @field:Json(name = "updated_at")
                val updatedAt: String,
                @field:Json(name = "username")
                val username: String
            ) {
                data class Links(
                    @field:Json(name = "followers")
                    val followers: String,
                    @field:Json(name = "following")
                    val following: String,
                    @field:Json(name = "html")
                    val html: String,
                    @field:Json(name = "likes")
                    val likes: String,
                    @field:Json(name = "photos")
                    val photos: String,
                    @field:Json(name = "portfolio")
                    val portfolio: String,
                    @field:Json(name = "self")
                    val self: String
                )
    
                data class ProfileImage(
                    @field:Json(name = "large")
                    val large: String,
                    @field:Json(name = "medium")
                    val medium: String,
                    @field:Json(name = "small")
                    val small: String
                )
            }
        }
    
        data class Urls(
            @field:Json(name = "full")
            val full: String,
            @field:Json(name = "raw")
            val raw: String,
            @field:Json(name = "regular")
            val regular: String,
            @field:Json(name = "small")
            val small: String,
            @field:Json(name = "thumb")
            val thumb: String
        )
    
        data class User(
            @field:Json(name = "accepted_tos")
            val acceptedTos: Boolean,
            @field:Json(name = "bio")
            val bio: String,
            @field:Json(name = "first_name")
            val firstName: String,
            @field:Json(name = "id")
            val id: String,
            @field:Json(name = "instagram_username")
            val instagramUsername: String,
            @field:Json(name = "last_name")
            val lastName: Any,
            @field:Json(name = "links")
            val links: Links,
            @field:Json(name = "location")
            val location: String,
            @field:Json(name = "name")
            val name: String,
            @field:Json(name = "portfolio_url")
            val portfolioUrl: String,
            @field:Json(name = "profile_image")
            val profileImage: ProfileImage,
            @field:Json(name = "total_collections")
            val totalCollections: Int,
            @field:Json(name = "total_likes")
            val totalLikes: Int,
            @field:Json(name = "total_photos")
            val totalPhotos: Int,
            @field:Json(name = "twitter_username")
            val twitterUsername: Any,
            @field:Json(name = "updated_at")
            val updatedAt: String,
            @field:Json(name = "username")
            val username: String
        ) {
            data class Links(
                @field:Json(name = "followers")
                val followers: String,
                @field:Json(name = "following")
                val following: String,
                @field:Json(name = "html")
                val html: String,
                @field:Json(name = "likes")
                val likes: String,
                @field:Json(name = "photos")
                val photos: String,
                @field:Json(name = "portfolio")
                val portfolio: String,
                @field:Json(name = "self")
                val self: String
            )
    
            data class ProfileImage(
                @field:Json(name = "large")
                val large: String,
                @field:Json(name = "medium")
                val medium: String,
                @field:Json(name = "small")
                val small: String
            )
        }
    }
}

fun PhotosResponse.PhotosResponseItem.asDomainModel(): Photo {
    return Photo(id = this.id, url = this.urls.thumb, name = this.user.username)
}
