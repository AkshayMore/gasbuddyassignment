package com.moreakshay.gasbuddyassignment.data.remote

import androidx.lifecycle.LiveData
import com.moreakshay.gasbuddyassignment.data.dtos.PhotoDetailsResponse
import com.moreakshay.gasbuddyassignment.data.dtos.PhotosResponse
import com.moreakshay.gasbuddyassignment.data.dtos.asDomainModel
import com.moreakshay.gasbuddyassignment.ui.domain.Photo
import com.moreakshay.gasbuddyassignment.ui.domain.PhotoDetails
import com.moreakshay.gasbuddyassignment.utils.network.NetworkBoundResource
import com.moreakshay.gasbuddyassignment.utils.network.Resource
import javax.inject.Inject

class GasBuddyRepository @Inject constructor(private val remote: ApiService){

    suspend fun getPhotos(): LiveData<Resource<List<Photo>>>{
        return object : NetworkBoundResource<List<Photo>, List<PhotosResponse.PhotosResponseItem>>(){
            override suspend fun createCall(): List<PhotosResponse.PhotosResponseItem> {
                return remote.getPhotos()
            }

            override suspend fun toDomainType(item: List<PhotosResponse.PhotosResponseItem>): List<Photo> {
                return item.map { it.asDomainModel() }
            }
        }.makeNetworkCall()
    }

    suspend fun getPhotoById(id: String): LiveData<Resource<PhotoDetails>>{
        return object : NetworkBoundResource<PhotoDetails, PhotoDetailsResponse>(){
            override suspend fun createCall(): PhotoDetailsResponse {
                return remote.getPhotoDetails(id)
            }
            override suspend fun toDomainType(item: PhotoDetailsResponse): PhotoDetails {
                return item.asDomainModel()
            }
        }.makeNetworkCall()
    }
}