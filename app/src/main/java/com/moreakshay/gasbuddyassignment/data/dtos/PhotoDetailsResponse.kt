package com.moreakshay.gasbuddyassignment.data.dtos

import com.moreakshay.gasbuddyassignment.ui.domain.PhotoDetails
import com.moreakshay.gasbuddyassignment.utils.constants.NOT_AVAILABLE
import com.squareup.moshi.Json

data class PhotoDetailsResponse(
    @Json(name = "alt_description")
    val altDescription: String?,
    @Json(name = "categories")
    val categories: List<Any?>?,
    @Json(name = "color")
    val color: String?,
    @Json(name = "created_at")
    val createdAt: String?,
    @Json(name = "current_user_collections")
    val currentUserCollections: List<Any?>?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "downloads")
    val downloads: Int?,
    @Json(name = "exif")
    val exif: Exif?,
    @Json(name = "height")
    val height: Int?,
    @Json(name = "id")
    val id: String?,
    @Json(name = "liked_by_user")
    val likedByUser: Boolean?,
    @Json(name = "likes")
    val likes: Int?,
    @Json(name = "links")
    val links: Links?,
    @Json(name = "location")
    val location: Location?,
    @Json(name = "meta")
    val meta: Meta?,
    @Json(name = "promoted_at")
    val promotedAt: String?,
    @Json(name = "related_collections")
    val relatedCollections: RelatedCollections?,
    @Json(name = "sponsorship")
    val sponsorship: Any?,
    @Json(name = "tags")
    val tags: List<Tag?>?,
    @Json(name = "updated_at")
    val updatedAt: String?,
    @Json(name = "urls")
    val urls: Urls?,
    @Json(name = "user")
    val user: User?,
    @Json(name = "views")
    val views: Int?,
    @Json(name = "width")
    val width: Int?
) {
    data class Exif(
        @Json(name = "aperture")
        val aperture: Any?,
        @Json(name = "exposure_time")
        val exposureTime: Any?,
        @Json(name = "focal_length")
        val focalLength: Any?,
        @Json(name = "iso")
        val iso: Any?,
        @Json(name = "make")
        val make: Any?,
        @Json(name = "model")
        val model: Any?
    )

    data class Links(
        @Json(name = "download")
        val download: String?,
        @Json(name = "download_location")
        val downloadLocation: String?,
        @Json(name = "html")
        val html: String?,
        @Json(name = "self")
        val self: String?
    )

    data class Location(
        @Json(name = "city")
        val city: String?,
        @Json(name = "country")
        val country: String?,
        @Json(name = "name")
        val name: String?,
        @Json(name = "position")
        val position: Position?,
        @Json(name = "title")
        val title: String?
    ) {
        data class Position(
            @Json(name = "latitude")
            val latitude: Double?,
            @Json(name = "longitude")
            val longitude: Double?
        )
    }

    data class Meta(
        @Json(name = "index")
        val index: Boolean?
    )

    data class RelatedCollections(
        @Json(name = "results")
        val results: List<Result?>?,
        @Json(name = "total")
        val total: Int?,
        @Json(name = "type")
        val type: String?
    ) {
        data class Result(
            @Json(name = "cover_photo")
            val coverPhoto: CoverPhoto?,
            @Json(name = "curated")
            val curated: Boolean?,
            @Json(name = "description")
            val description: String?,
            @Json(name = "featured")
            val featured: Boolean?,
            @Json(name = "id")
            val id: Int?,
            @Json(name = "last_collected_at")
            val lastCollectedAt: String?,
            @Json(name = "links")
            val links: Links?,
            @Json(name = "preview_photos")
            val previewPhotos: List<PreviewPhoto?>?,
            @Json(name = "private")
            val `private`: Boolean?,
            @Json(name = "published_at")
            val publishedAt: String?,
            @Json(name = "share_key")
            val shareKey: String?,
            @Json(name = "tags")
            val tags: List<Tag?>?,
            @Json(name = "title")
            val title: String?,
            @Json(name = "total_photos")
            val totalPhotos: Int?,
            @Json(name = "updated_at")
            val updatedAt: String?,
            @Json(name = "user")
            val user: User?
        ) {
            data class CoverPhoto(
                @Json(name = "alt_description")
                val altDescription: String?,
                @Json(name = "categories")
                val categories: List<Any?>?,
                @Json(name = "color")
                val color: String?,
                @Json(name = "created_at")
                val createdAt: String?,
                @Json(name = "current_user_collections")
                val currentUserCollections: List<Any?>?,
                @Json(name = "description")
                val description: String?,
                @Json(name = "height")
                val height: Int?,
                @Json(name = "id")
                val id: String?,
                @Json(name = "liked_by_user")
                val likedByUser: Boolean?,
                @Json(name = "likes")
                val likes: Int?,
                @Json(name = "links")
                val links: Links?,
                @Json(name = "promoted_at")
                val promotedAt: String?,
                @Json(name = "sponsorship")
                val sponsorship: Any?,
                @Json(name = "updated_at")
                val updatedAt: String?,
                @Json(name = "urls")
                val urls: Urls?,
                @Json(name = "user")
                val user: User?,
                @Json(name = "width")
                val width: Int?
            ) {
                data class Links(
                    @Json(name = "download")
                    val download: String?,
                    @Json(name = "download_location")
                    val downloadLocation: String?,
                    @Json(name = "html")
                    val html: String?,
                    @Json(name = "self")
                    val self: String?
                )

                data class Urls(
                    @Json(name = "full")
                    val full: String?,
                    @Json(name = "raw")
                    val raw: String?,
                    @Json(name = "regular")
                    val regular: String?,
                    @Json(name = "small")
                    val small: String?,
                    @Json(name = "thumb")
                    val thumb: String?
                )

                data class User(
                    @Json(name = "accepted_tos")
                    val acceptedTos: Boolean?,
                    @Json(name = "bio")
                    val bio: Any?,
                    @Json(name = "first_name")
                    val firstName: String?,
                    @Json(name = "id")
                    val id: String?,
                    @Json(name = "instagram_username")
                    val instagramUsername: Any?,
                    @Json(name = "last_name")
                    val lastName: String?,
                    @Json(name = "links")
                    val links: Links?,
                    @Json(name = "location")
                    val location: Any?,
                    @Json(name = "name")
                    val name: String?,
                    @Json(name = "portfolio_url")
                    val portfolioUrl: Any?,
                    @Json(name = "profile_image")
                    val profileImage: ProfileImage?,
                    @Json(name = "total_collections")
                    val totalCollections: Int?,
                    @Json(name = "total_likes")
                    val totalLikes: Int?,
                    @Json(name = "total_photos")
                    val totalPhotos: Int?,
                    @Json(name = "twitter_username")
                    val twitterUsername: String?,
                    @Json(name = "updated_at")
                    val updatedAt: String?,
                    @Json(name = "username")
                    val username: String?
                ) {
                    data class Links(
                        @Json(name = "followers")
                        val followers: String?,
                        @Json(name = "following")
                        val following: String?,
                        @Json(name = "html")
                        val html: String?,
                        @Json(name = "likes")
                        val likes: String?,
                        @Json(name = "photos")
                        val photos: String?,
                        @Json(name = "portfolio")
                        val portfolio: String?,
                        @Json(name = "self")
                        val self: String?
                    )

                    data class ProfileImage(
                        @Json(name = "large")
                        val large: String?,
                        @Json(name = "medium")
                        val medium: String?,
                        @Json(name = "small")
                        val small: String?
                    )
                }
            }

            data class Links(
                @Json(name = "html")
                val html: String?,
                @Json(name = "photos")
                val photos: String?,
                @Json(name = "related")
                val related: String?,
                @Json(name = "self")
                val self: String?
            )

            data class PreviewPhoto(
                @Json(name = "created_at")
                val createdAt: String?,
                @Json(name = "id")
                val id: String?,
                @Json(name = "updated_at")
                val updatedAt: String?,
                @Json(name = "urls")
                val urls: Urls?
            ) {
                data class Urls(
                    @Json(name = "full")
                    val full: String?,
                    @Json(name = "raw")
                    val raw: String?,
                    @Json(name = "regular")
                    val regular: String?,
                    @Json(name = "small")
                    val small: String?,
                    @Json(name = "thumb")
                    val thumb: String?
                )
            }

            data class Tag(
                @Json(name = "source")
                val source: Source?,
                @Json(name = "title")
                val title: String?,
                @Json(name = "type")
                val type: String?
            ) {
                data class Source(
                    @Json(name = "ancestry")
                    val ancestry: Ancestry?,
                    @Json(name = "cover_photo")
                    val coverPhoto: CoverPhoto?,
                    @Json(name = "description")
                    val description: String?,
                    @Json(name = "meta_description")
                    val metaDescription: String?,
                    @Json(name = "meta_title")
                    val metaTitle: String?,
                    @Json(name = "subtitle")
                    val subtitle: String?,
                    @Json(name = "title")
                    val title: String?
                ) {
                    data class Ancestry(
                        @Json(name = "category")
                        val category: Category?,
                        @Json(name = "subcategory")
                        val subcategory: Subcategory?,
                        @Json(name = "type")
                        val type: Type?
                    ) {
                        data class Category(
                            @Json(name = "pretty_slug")
                            val prettySlug: String?,
                            @Json(name = "slug")
                            val slug: String?
                        )

                        data class Subcategory(
                            @Json(name = "pretty_slug")
                            val prettySlug: String?,
                            @Json(name = "slug")
                            val slug: String?
                        )

                        data class Type(
                            @Json(name = "pretty_slug")
                            val prettySlug: String?,
                            @Json(name = "slug")
                            val slug: String?
                        )
                    }

                    data class CoverPhoto(
                        @Json(name = "alt_description")
                        val altDescription: String?,
                        @Json(name = "categories")
                        val categories: List<Any?>?,
                        @Json(name = "color")
                        val color: String?,
                        @Json(name = "created_at")
                        val createdAt: String?,
                        @Json(name = "current_user_collections")
                        val currentUserCollections: List<Any?>?,
                        @Json(name = "description")
                        val description: Any?,
                        @Json(name = "height")
                        val height: Int?,
                        @Json(name = "id")
                        val id: String?,
                        @Json(name = "liked_by_user")
                        val likedByUser: Boolean?,
                        @Json(name = "likes")
                        val likes: Int?,
                        @Json(name = "links")
                        val links: Links?,
                        @Json(name = "promoted_at")
                        val promotedAt: String?,
                        @Json(name = "sponsorship")
                        val sponsorship: Any?,
                        @Json(name = "updated_at")
                        val updatedAt: String?,
                        @Json(name = "urls")
                        val urls: Urls?,
                        @Json(name = "user")
                        val user: User?,
                        @Json(name = "width")
                        val width: Int?
                    ) {
                        data class Links(
                            @Json(name = "download")
                            val download: String?,
                            @Json(name = "download_location")
                            val downloadLocation: String?,
                            @Json(name = "html")
                            val html: String?,
                            @Json(name = "self")
                            val self: String?
                        )

                        data class Urls(
                            @Json(name = "full")
                            val full: String?,
                            @Json(name = "raw")
                            val raw: String?,
                            @Json(name = "regular")
                            val regular: String?,
                            @Json(name = "small")
                            val small: String?,
                            @Json(name = "thumb")
                            val thumb: String?
                        )

                        data class User(
                            @Json(name = "accepted_tos")
                            val acceptedTos: Boolean?,
                            @Json(name = "bio")
                            val bio: String?,
                            @Json(name = "first_name")
                            val firstName: String?,
                            @Json(name = "id")
                            val id: String?,
                            @Json(name = "instagram_username")
                            val instagramUsername: String?,
                            @Json(name = "last_name")
                            val lastName: String?,
                            @Json(name = "links")
                            val links: Links?,
                            @Json(name = "location")
                            val location: String?,
                            @Json(name = "name")
                            val name: String?,
                            @Json(name = "portfolio_url")
                            val portfolioUrl: String?,
                            @Json(name = "profile_image")
                            val profileImage: ProfileImage?,
                            @Json(name = "total_collections")
                            val totalCollections: Int?,
                            @Json(name = "total_likes")
                            val totalLikes: Int?,
                            @Json(name = "total_photos")
                            val totalPhotos: Int?,
                            @Json(name = "twitter_username")
                            val twitterUsername: String?,
                            @Json(name = "updated_at")
                            val updatedAt: String?,
                            @Json(name = "username")
                            val username: String?
                        ) {
                            data class Links(
                                @Json(name = "followers")
                                val followers: String?,
                                @Json(name = "following")
                                val following: String?,
                                @Json(name = "html")
                                val html: String?,
                                @Json(name = "likes")
                                val likes: String?,
                                @Json(name = "photos")
                                val photos: String?,
                                @Json(name = "portfolio")
                                val portfolio: String?,
                                @Json(name = "self")
                                val self: String?
                            )

                            data class ProfileImage(
                                @Json(name = "large")
                                val large: String?,
                                @Json(name = "medium")
                                val medium: String?,
                                @Json(name = "small")
                                val small: String?
                            )
                        }
                    }
                }
            }

            data class User(
                @Json(name = "accepted_tos")
                val acceptedTos: Boolean?,
                @Json(name = "bio")
                val bio: String?,
                @Json(name = "first_name")
                val firstName: String?,
                @Json(name = "id")
                val id: String?,
                @Json(name = "instagram_username")
                val instagramUsername: Any?,
                @Json(name = "last_name")
                val lastName: String?,
                @Json(name = "links")
                val links: Links?,
                @Json(name = "location")
                val location: String?,
                @Json(name = "name")
                val name: String?,
                @Json(name = "portfolio_url")
                val portfolioUrl: String?,
                @Json(name = "profile_image")
                val profileImage: ProfileImage?,
                @Json(name = "total_collections")
                val totalCollections: Int?,
                @Json(name = "total_likes")
                val totalLikes: Int?,
                @Json(name = "total_photos")
                val totalPhotos: Int?,
                @Json(name = "twitter_username")
                val twitterUsername: String?,
                @Json(name = "updated_at")
                val updatedAt: String?,
                @Json(name = "username")
                val username: String?
            ) {
                data class Links(
                    @Json(name = "followers")
                    val followers: String?,
                    @Json(name = "following")
                    val following: String?,
                    @Json(name = "html")
                    val html: String?,
                    @Json(name = "likes")
                    val likes: String?,
                    @Json(name = "photos")
                    val photos: String?,
                    @Json(name = "portfolio")
                    val portfolio: String?,
                    @Json(name = "self")
                    val self: String?
                )

                data class ProfileImage(
                    @Json(name = "large")
                    val large: String?,
                    @Json(name = "medium")
                    val medium: String?,
                    @Json(name = "small")
                    val small: String?
                )
            }
        }
    }

    data class Tag(
        @Json(name = "source")
        val source: Source?,
        @Json(name = "title")
        val title: String?,
        @Json(name = "type")
        val type: String?
    ) {
        data class Source(
            @Json(name = "ancestry")
            val ancestry: Ancestry?,
            @Json(name = "cover_photo")
            val coverPhoto: CoverPhoto?,
            @Json(name = "description")
            val description: String?,
            @Json(name = "meta_description")
            val metaDescription: String?,
            @Json(name = "meta_title")
            val metaTitle: String?,
            @Json(name = "subtitle")
            val subtitle: String?,
            @Json(name = "title")
            val title: String?
        ) {
            data class Ancestry(
                @Json(name = "category")
                val category: Category?,
                @Json(name = "subcategory")
                val subcategory: Subcategory?,
                @Json(name = "type")
                val type: Type?
            ) {
                data class Category(
                    @Json(name = "pretty_slug")
                    val prettySlug: String?,
                    @Json(name = "slug")
                    val slug: String?
                )

                data class Subcategory(
                    @Json(name = "pretty_slug")
                    val prettySlug: String?,
                    @Json(name = "slug")
                    val slug: String?
                )

                data class Type(
                    @Json(name = "pretty_slug")
                    val prettySlug: String?,
                    @Json(name = "slug")
                    val slug: String?
                )
            }

            data class CoverPhoto(
                @Json(name = "alt_description")
                val altDescription: String?,
                @Json(name = "categories")
                val categories: List<Any?>?,
                @Json(name = "color")
                val color: String?,
                @Json(name = "created_at")
                val createdAt: String?,
                @Json(name = "current_user_collections")
                val currentUserCollections: List<Any?>?,
                @Json(name = "description")
                val description: Any?,
                @Json(name = "height")
                val height: Int?,
                @Json(name = "id")
                val id: String?,
                @Json(name = "liked_by_user")
                val likedByUser: Boolean?,
                @Json(name = "likes")
                val likes: Int?,
                @Json(name = "links")
                val links: Links?,
                @Json(name = "promoted_at")
                val promotedAt: String?,
                @Json(name = "sponsorship")
                val sponsorship: Any?,
                @Json(name = "updated_at")
                val updatedAt: String?,
                @Json(name = "urls")
                val urls: Urls?,
                @Json(name = "user")
                val user: User?,
                @Json(name = "width")
                val width: Int?
            ) {
                data class Links(
                    @Json(name = "download")
                    val download: String?,
                    @Json(name = "download_location")
                    val downloadLocation: String?,
                    @Json(name = "html")
                    val html: String?,
                    @Json(name = "self")
                    val self: String?
                )

                data class Urls(
                    @Json(name = "full")
                    val full: String?,
                    @Json(name = "raw")
                    val raw: String?,
                    @Json(name = "regular")
                    val regular: String?,
                    @Json(name = "small")
                    val small: String?,
                    @Json(name = "thumb")
                    val thumb: String?
                )

                data class User(
                    @Json(name = "accepted_tos")
                    val acceptedTos: Boolean?,
                    @Json(name = "bio")
                    val bio: String?,
                    @Json(name = "first_name")
                    val firstName: String?,
                    @Json(name = "id")
                    val id: String?,
                    @Json(name = "instagram_username")
                    val instagramUsername: String?,
                    @Json(name = "last_name")
                    val lastName: String?,
                    @Json(name = "links")
                    val links: Links?,
                    @Json(name = "location")
                    val location: String?,
                    @Json(name = "name")
                    val name: String?,
                    @Json(name = "portfolio_url")
                    val portfolioUrl: String?,
                    @Json(name = "profile_image")
                    val profileImage: ProfileImage?,
                    @Json(name = "total_collections")
                    val totalCollections: Int?,
                    @Json(name = "total_likes")
                    val totalLikes: Int?,
                    @Json(name = "total_photos")
                    val totalPhotos: Int?,
                    @Json(name = "twitter_username")
                    val twitterUsername: String?,
                    @Json(name = "updated_at")
                    val updatedAt: String?,
                    @Json(name = "username")
                    val username: String?
                ) {
                    data class Links(
                        @Json(name = "followers")
                        val followers: String?,
                        @Json(name = "following")
                        val following: String?,
                        @Json(name = "html")
                        val html: String?,
                        @Json(name = "likes")
                        val likes: String?,
                        @Json(name = "photos")
                        val photos: String?,
                        @Json(name = "portfolio")
                        val portfolio: String?,
                        @Json(name = "self")
                        val self: String?
                    )

                    data class ProfileImage(
                        @Json(name = "large")
                        val large: String?,
                        @Json(name = "medium")
                        val medium: String?,
                        @Json(name = "small")
                        val small: String?
                    )
                }
            }
        }
    }

    data class Urls(
        @Json(name = "full")
        val full: String?,
        @Json(name = "raw")
        val raw: String?,
        @Json(name = "regular")
        val regular: String?,
        @Json(name = "small")
        val small: String?,
        @Json(name = "thumb")
        val thumb: String?
    )

    data class User(
        @Json(name = "accepted_tos")
        val acceptedTos: Boolean?,
        @Json(name = "bio")
        val bio: Any?,
        @Json(name = "first_name")
        val firstName: String?,
        @Json(name = "id")
        val id: String?,
        @Json(name = "instagram_username")
        val instagramUsername: Any?,
        @Json(name = "last_name")
        val lastName: String?,
        @Json(name = "links")
        val links: Links?,
        @Json(name = "location")
        val location: Any?,
        @Json(name = "name")
        val name: String?,
        @Json(name = "portfolio_url")
        val portfolioUrl: Any?,
        @Json(name = "profile_image")
        val profileImage: ProfileImage?,
        @Json(name = "total_collections")
        val totalCollections: Int?,
        @Json(name = "total_likes")
        val totalLikes: Int?,
        @Json(name = "total_photos")
        val totalPhotos: Int?,
        @Json(name = "twitter_username")
        val twitterUsername: String?,
        @Json(name = "updated_at")
        val updatedAt: String?,
        @Json(name = "username")
        val username: String?
    ) {
        data class Links(
            @Json(name = "followers")
            val followers: String?,
            @Json(name = "following")
            val following: String?,
            @Json(name = "html")
            val html: String?,
            @Json(name = "likes")
            val likes: String?,
            @Json(name = "photos")
            val photos: String?,
            @Json(name = "portfolio")
            val portfolio: String?,
            @Json(name = "self")
            val self: String?
        )

        data class ProfileImage(
            @Json(name = "large")
            val large: String?,
            @Json(name = "medium")
            val medium: String?,
            @Json(name = "small")
            val small: String?
        )
    }
}

fun PhotoDetailsResponse.asDomainModel(): PhotoDetails {
    return PhotoDetails(
        id = this.id ?: NOT_AVAILABLE,
        url = this.urls?.full ?: NOT_AVAILABLE,
        name = this.user?.username ?: NOT_AVAILABLE,
        location = this.location?.name ?: NOT_AVAILABLE,
        date = this.createdAt ?: NOT_AVAILABLE,
        likes = this.likes ?: 0,
        description = this.description ?: NOT_AVAILABLE
        )
}
