package com.moreakshay.gasbuddyassignment

import android.app.Application
import com.moreakshay.gasbuddyassignment.injection.component.AppComponent
import com.moreakshay.gasbuddyassignment.injection.component.DaggerAppComponent
import com.moreakshay.gasbuddyassignment.injection.modules.AppModule

class GasBuddyApplication: Application() {

    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()
        buildAppComponent()
    }

    private fun buildAppComponent() {
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}